package ru.mtumanov.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("ERROR! Number is incorrect!");
    }

    public NumberIncorrectException(@NotNull final String value) {
        super("ERROR! Value: \"" + value + "\" is incorrect!");
    }

}
