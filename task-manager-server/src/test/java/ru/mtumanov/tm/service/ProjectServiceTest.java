package ru.mtumanov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.dto.IDtoProjectService;
import ru.mtumanov.tm.api.service.dto.IDtoUserService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.migration.AbstractSchemeTest;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.service.dto.ProjectDtoService;
import ru.mtumanov.tm.service.dto.UserDtoService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectServiceTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static UserDTO USER1;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IDtoUserService userService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private static final IDtoProjectService projectService = new ProjectDtoService(connectionService);

    @NotNull
    private static final List<ProjectDTO> projectList = new ArrayList<>();

    @BeforeClass
    public static void init() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        USER1 = userService.create("UserFirstName 10", "password", Role.USUAL);
        try {
            entityManager.getTransaction().begin();
            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull final ProjectDTO project = new ProjectDTO();
                project.setName("ProjectDTO name: " + i);
                project.setDescription("ProjectDTO description: " + i);
                project.setUserId(USER1.getId());
                projectRepository.add(project);
                projectList.add(project);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void clearRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        userService.removeByLogin(USER1.getLogin());
        projectList.clear();
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test project name");
        project.setDescription("Test project description");
        project.setUserId(USER1.getId());

        projectList.add(project);
        projectService.add(USER1.getId(), project);
        List<ProjectDTO> actualProjectList = projectService.findAll();
        assertEquals(projectList.size(), actualProjectList.size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionAdd() throws Exception {
        projectService.add("", new ProjectDTO());
    }

    @Test
    public void testClear() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        assertNotEquals(0, projectRepository.findAll(USER1.getId()).size());
        projectService.clear(USER1.getId());
        assertEquals(0, projectRepository.findAll(USER1.getId()).size());
    }

    @Test
    public void TestfindAll() throws Exception {
        @NotNull final List<ProjectDTO> projectsUser1 = new ArrayList<>();
        for (@NotNull final ProjectDTO project : projectList) {
            projectsUser1.add(project);
        }

        @NotNull final List<ProjectDTO> actualProjectsUser1 = projectService.findAll(USER1.getId());
        @NotNull final List<ProjectDTO> actualProjectsAll = projectService.findAll();
        assertEquals(projectsUser1, actualProjectsUser1);
        assertEquals(actualProjectsAll.size(), projectList.size());
    }

    @Test
    public void testFindOneById() throws Exception {
        for (@NotNull final ProjectDTO project : projectList) {
            assertEquals(project, projectService.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testRemove() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        for (@NotNull final ProjectDTO project : projectList) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
            projectService.remove(project.getUserId(), project);
            assertFalse(projectRepository.existById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testRemoveById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        for (@NotNull final ProjectDTO project : projectList) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
            projectService.removeById(project.getUserId(), project.getId());
            assertFalse(projectRepository.existById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testExistById() throws Exception {
        for (@NotNull final ProjectDTO project : projectList) {
            assertTrue(projectService.existById(project.getUserId(), project.getId()));
        }
        assertFalse(projectService.existById(USER1.getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void testGetSize() throws Exception {
        @NotNull final List<ProjectDTO> projectsUser1 = new ArrayList<>();
        for (@NotNull final ProjectDTO project : projectList) {
            projectsUser1.add(project);
        }
        assertEquals(projectsUser1.size(), projectService.getSize(USER1.getId()));
    }

    @Test
    public void testChangeProjectStatusById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        for (@NotNull final ProjectDTO project : projectList) {
            projectService.changeProjectStatusById(project.getUserId(), project.getId(), Status.IN_PROGRESS);
            @NotNull final ProjectDTO actualProject = projectRepository.findOneById(project.getUserId(), project.getId());
            assertEquals(Status.IN_PROGRESS, actualProject.getStatus());
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testExceptionChangeProjectStatusById() throws Exception {
        projectService.changeProjectStatusById(UUID.randomUUID().toString(), "", Status.IN_PROGRESS);
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final String name = "TEST project";
        @NotNull final String description = "DEscription";
        @NotNull final ProjectDTO project = projectService.create(USER1.getId(), name, description);
        @NotNull final ProjectDTO actualProject = projectRepository.findOneById(project.getUserId(), project.getId());
        assertEquals(name, actualProject.getName());
        assertEquals(description, actualProject.getDescription());
        assertEquals(USER1.getId(), actualProject.getUserId());
        assertEquals(projectRepository.getSize(project.getUserId()), projectList.size() + 1);
    }

    @Test
    public void testUpdateById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        for (@NotNull final ProjectDTO project : projectList) {
            @NotNull final String name = project.getName() + "TEST";
            @NotNull final String description = project.getDescription() + "TEST";
            projectService.updateById(project.getUserId(), project.getId(), name, description);
            @NotNull final ProjectDTO actualProject = projectRepository.findOneById(project.getUserId(), project.getId());
            assertEquals(name, actualProject.getName());
            assertEquals(description, actualProject.getDescription());
        }
    }

    @Test(expected = NameEmptyException.class)
    public void testExceptionUpdateById() throws Exception {
        projectService.updateById(UUID.randomUUID().toString(), UUID.randomUUID().toString(), "", "");
    }

}
