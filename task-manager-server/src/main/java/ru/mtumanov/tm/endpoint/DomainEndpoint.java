package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IDomainEndpoint;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.request.data.*;
import ru.mtumanov.tm.dto.response.data.*;
import ru.mtumanov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.mtumanov.tm.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public DataBase64LoadRs loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataBase64();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBase64LoadRs(e);
        }
        return new DataBase64LoadRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataBase64SaveRs saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataBase64();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBase64SaveRs(e);
        }
        return new DataBase64SaveRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataBinaryLoadRs loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataBinary();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBinaryLoadRs(e);
        }
        return new DataBinaryLoadRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataBinarySaveRs saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataBinary();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBinarySaveRs(e);
        }
        return new DataBinarySaveRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataJsonLoadFasterXmlRs loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXmlRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataJsonFasterXml();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonLoadFasterXmlRs(e);
        }
        return new DataJsonLoadFasterXmlRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataJsonSaveFasterXmlRs saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXmlRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataJsonFasterXml();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonSaveFasterXmlRs(e);
        }
        return new DataJsonSaveFasterXmlRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataJsonLoadJaxbRs loadDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxbRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataJsonJaxb();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonLoadJaxbRs(e);
        }
        return new DataJsonLoadJaxbRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataJsonSaveJaxbRs saveDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxbRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataJsonJaxb();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonSaveJaxbRs(e);
        }
        return new DataJsonSaveJaxbRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataXmlLoadFasterXmlRs loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadFasterXmlRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataXmlFasterXml();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlLoadFasterXmlRs(e);
        }
        return new DataXmlLoadFasterXmlRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataXmlSaveFasterXmlRs saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveFasterXmlRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataXmlFasterXml();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlSaveFasterXmlRs(e);
        }
        return new DataXmlSaveFasterXmlRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataXmlLoadJaxbRs loadDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadJaxbRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataXmlJaxb();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlLoadJaxbRs(e);
        }
        return new DataXmlLoadJaxbRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataXmlSaveJaxbRs saveDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveJaxbRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataXmlJaxb();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlSaveJaxbRs(e);
        }
        return new DataXmlSaveJaxbRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataYamlLoadFasterXmlRs loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXmlRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataYamlFasterXml();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataYamlLoadFasterXmlRs(e);
        }
        return new DataYamlLoadFasterXmlRs();
    }

    @Override
    @NotNull
    @WebMethod
    public DataYamlSaveFasterXmlRs saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXmlRq request
    ) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataYamlFasterXml();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataYamlSaveFasterXmlRs(e);
        }
        return new DataYamlSaveFasterXmlRs();
    }

}
