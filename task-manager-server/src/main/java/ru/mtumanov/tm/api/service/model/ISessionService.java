package ru.mtumanov.tm.api.service.model;

import ru.mtumanov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
